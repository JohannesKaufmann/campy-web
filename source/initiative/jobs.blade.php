@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple">
    <div class="text-3xl mb-4 text-white">Jobs</div>
    <div class="p-8 rounded bg-white">
        <h2><strong>Über Code+Design Initiative e.V.</strong></h2>

        <ul>
            <li>Wir sind ein gemeinnütziger Verein
            <li>Unsere Mission: Wir begeistern Jugendliche für die digitalen Berufe – vor allem Mädels
            <li>Wir organisieren Events mit bis zu 100 Jugendlichen in ganz Deutschland
            <li>Auf den Events erleben die Jugendliche die Arbeitswelt von morgen schon heute: im Team entwickeln sie digitale Produkte</li>
        </ul>

        <div class="text-2xl mt-8">Offene Stellen</div>


            <p>Leider gibt es aktuell keine offenen Stellen.</p>

        </div>

    </div>
</main>
@endsection

@section('title')
Jobs
@endsection
